/**
 * Created by �� on 03.07.2015.
 */
public interface Vector extends Bytable
{
	Type getValue( int i );
	void setValue( int i , Type v );
	int getSize();
	Type dot( final Vector vec );
	Vector add( Vector vec );
	Vector sub( Vector vec );
	Vector cross( Vector vec );
	//Vector rotate( List< Double > angles );
	Vector scale( Type k );
	Vector normalize();
	double length();
}
