/**
 * Created by �� on 03.07.2015.
 */
//N * M matrix of type Type
public interface Matrix extends Progressable , Bytable
{
	//class holds results for lup
	public class SHLUP
	{
		public Matrix _L;
		public Matrix _U;
		public int[] _row;
		public int[] _col;
		public Type _det;
	}
	/**
	 *returns _data[ i * N + j ]
	 */
	Type getValue( int i , int j );
	void setValue( int i , int j , Type data );
	/**
	 *returns dimensions N , M
	 */
	int[] getDim();
	/**
	 *return determinant
	 */
	Type det();
	/**
	 *return matrix at the intersection of rows and cols
	 */
	Matrix subMatrix( int[] rows , int[] cols );
	/**
	 *calculate LUP factorization
	 */
	SHLUP LU();
	/**
	 *return vector with size M of ith row
	 */
	Vector getRow( int i );
	/**
	 *return vector with size N of jth collum
	 */
	Vector getCol( int j );
	/**
	 *return result of matrix multiplication, mat should me M*P size while *this is N*M size
	 */
	Matrix mul( Matrix mat );
	/**
	 *return result of matrix multiplication by vector, vector should me M size while *this is N*M size
	 */
	Matrix mul( Vector vec );
	/**
	 *return matrix scaled by argument
	 */
	Matrix scale( Type k );
	/**
	 *return result of matrix addition, mat should me N*M size while *this is N*M size
	 */
	Matrix add( Matrix mat );
	Matrix sub( Matrix mat );
	/**
	 *solve A * x = c
	 */
	Vector solve( Vector c );
	/**
	 *excludes ith row and jth col from calculations and decrement N and M
	 */
	void exclude( int i , int j );
	/**
	 *includes ith row and jth col if they are excluded
	 */
	void include( int i , int j );
	/**
	 *adds row and col at the end of matrix and increments N and M
	 */
	void add();
	/**
	 *replace ith rows nth elements with vector[n] starting at start a1|a2|a3|v1|v2|v3|a7|a8
	 */
	void setRow( int i , int start , Vector row );
	/**
	 *replace jth collums nth elements with vector[n] starting at start a1|a2|a3|v1|v2|v3|a7|a8
	 */
	void setCol( int j , int start , Vector col );
	/**
	 * @return copy of inner array
	 */
	Type[] copyArray();
	/**
	 *
	 * @return transposed copy of inner array
	 */
	Type[] copyArrayT();
 	/**
	 *reset inner array to argument
	 */
	void reset( Type[][] new_data );
	/**
	 *return inverse matrix
	 */
	Matrix getInverse();
	void print();
}
