import java.util.List;
/**
 * Created by �� on 03.07.2015.
 */
public interface Progressable
{
	/**
	 * get progress of currrent operation 0.0..1.0
	 */
	double getProgress();
	/**
	 * cancel current operation
	 */
	void cancel();
	/**
	 * get duration of the last operation
	 * @return time in seconds
	 */
	double getLastDur();
}
