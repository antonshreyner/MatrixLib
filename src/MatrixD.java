/**
 * Created by �� on 04.07.2015.
 * dence matrix
 */
public class MatrixD implements Matrix
{
	private Type[] _data;
	private int N;
	private int M;
	/**
	 *row and col order
	 */
	private int[] _row;
	private int[] _col;
	/**
	 * performance metrics
	 */
	boolean cancel = false;
	double duration = 0.0;

	@Override
	public Type getValue( int i, int j )
	{
		return _data[ _row[ i ] * M + _col[ j ] ];
	}
	@Override
	public void setValue( int i , int j , Type data )
	{
		_data[ _row[ i ] * M + _col[ j ] ].copyIn( data );
	}
	@Override
	public int[] getDim()
	{
		int[] out = { N , M };
		return out;
	}
	MatrixD( Type[] in, int n, int m )
	{
		N = n;
		M = m;
		_row = new int[ N ];
		_col = new int[ M ];
		_data = in[ 0 ].genArray( n * m );
		for( int i = 0; i < N; i ++ )
		{
			_row[ i ] = i;
			for( int j = 0; j < M; j++ )
			{
				if( i == 0 )
				{
					_col[ j ] = j;
				}
				setValue( i , j , in[ i * m + j ] );
			}
		}
	}
	@Override
	public void print()
	{
		for( int i = 0; i < N; i ++ )
		{
			for( int j = 0; j < M; j++ )
			{
				System.out.print( getValue( i , j ).toStr() + " " );
			}
			System.out.print( "\n" );
		}
	}
	@Override
	public Type det()
	{
		if( N != M )
			return null;
		Type det = _data[ 0 ].genSame();
		det.unit();
		Type[] temp = copyArray();
		long t0 = System.currentTimeMillis();
		for( int n = 0; n < N; n++ )
		{
			Type ann = temp[ n * N + n ].copy();
			if( ann.isZero() )
				return null;
			det.copyIn( det.mul( ann ) );
			for( int i = n + 1; i < N; i ++ )
			{
				Type ain = temp[ i * N + n ].div( ann );
				for( int j = i; j < N; j ++ )
				{
					temp[ i * N + j ].copyIn( temp[ i * N + j ].sub( temp[ n * N + j ].mul( ain ) ) );
				}
			}
		}
		long t1 = System.currentTimeMillis();
		duration = ( double )( t1 - t0 ) * 0.001;
		return det;
	}
	@Override
	public Matrix subMatrix( int[] rows, int[] cols )
	{
		if( rows.length > N || cols.length > M )
			return null;
		long t0 = System.currentTimeMillis();
		Type[] data = _data[ 0 ].genArray( rows.length * cols.length );
		for( int i = 0; i < rows.length; i ++ )
		{
			for( int j = 0; j < cols.length; j++ )
			{
				data[ i * cols.length + j ].copyIn( getValue( rows[ i ], cols[ j ] ) );
			}
		}
		long t1 = System.currentTimeMillis();
		duration = ( double )( t1 - t0 ) * 0.001;
		return new MatrixD( data , rows.length , cols.length );
	}
	@Override
	public SHLUP LU()
	{
		return null;
	}
	@Override
	public Vector getRow( int i )
	{
		return null;
	}
	@Override
	public Vector getCol( int j )
	{
		return null;
	}
	@Override
	public Matrix mul( Matrix mat )
	{
		return null;
	}
	@Override
	public Matrix mul( Vector vec )
	{
		return null;
	}
	@Override
	public Matrix scale( Type k )
	{
		return null;
	}
	@Override
	public Matrix add( Matrix mat )
	{
		return null;
	}
	@Override
	public Matrix sub( Matrix mat )
	{
		return null;
	}
	@Override
	public Vector solve( Vector c )
	{
		return null;
	}
	@Override
	public void exclude( int i, int j )
	{
	}
	@Override
	public void include( int i, int j )
	{
	}
	@Override
	public void add()
	{
	}
	@Override
	public void setRow( int i, int start, Vector row )
	{
	}
	@Override
	public void setCol( int j, int start, Vector col )
	{
	}
	@Override
	public Type[] copyArray()
	{
		Type[] out = _data[ 0 ].genArray( N * M );
		for( int i = 0; i < N * M; i ++ )
		{
			out[ i ].copyIn( _data[ i ] );
		}
		return out;
	}
	@Override
	public Type[] copyArrayT()
	{
		Type[] out = _data[ 0 ].genArray( N * M );
		for( int i = 0; i < N; i ++ )
		{
			for( int j = 0; j < M; j ++ )
			{
				out[ j * M + i ].copyIn( _data[ i * N + j ] );
			}
		}
		return out;
	}
	@Override
	public void reset( Type[][] new_data )
	{
	}
	@Override
	public Matrix getInverse()
	{
		return null;
	}
	@Override
	public double getProgress()
	{
		return 0;
	}
	@Override
	public void cancel()
	{
	}
	@Override
	public double getLastDur()
	{
		return duration;
	}
	@Override
	public byte[] getByteCode()
	{
		return new byte[ 0 ];
	}
}
