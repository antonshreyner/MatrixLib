/**
 * Created by �� on 04.07.2015.
 */
public interface Type extends Bytable
{
	public enum LinTypes
	{
		DOUBLE , COMPLEX , INTEGER;
	}
	void copyIn( Type a );
	Type copy();
	Type add( Type a );
	Type sub( Type a );
	double length();
	Type normalize();
	Type scale( double k );
	Type mul( Type a );
	Type div( Type a );
	Type inverse();
	LinTypes getType();
	Type[] genArray( int n );
	Type genSame();
	void unit();
	String toStr();
	boolean isZero();
}
