import org.jblas.*;

import java.io.PrintWriter;
public class Main
{
	static
	{
		//System.loadLibrary( "cudaMatrixSolverJava" );
		System.loadLibrary( "JavaMatrixSolver" );
	}

	public static native double calcDet( final double[] mat, int N );
	public static native void nativeTest();
	public static native void init( int N );
	public static native void release();
	public static void main( String[] argv )
	{
		/*
		int N = 2000;
		init( N );
		Matrix A = Matrix.random( N, N );
		for( int i = 0; i < N; i++ )
		{
			for( int j = 0; j < N; j++ )
			{
				if( Math.abs( j - i ) > 1 )
					A.set( i , j , 0.0 );
			}
		}
		double det = A.det();
		float jama_test_time = Main.runTest( A , C, N, TestTypeDet, TestSourceJama );
		System.out.print( "Jama: " + N + "x" + N + " matrix Det takes avg " + jama_test_time + " millis\n" );
		float mine_test_time = Main.runTest( A , C, N, TestTypeDet , TestSourceMine );
		System.out.print( "Mine: " + N + "x" + N + " matrix Det takes avg " + mine_test_time + " millis\n" );
		//nativeTest();
		release();*/
		final int N = 30;
		final int iter = 5;
		final int step = 100;
		/*DoubleType prot = new DoubleType( 0.0 );
		Type[] data = prot.genArray( N * N );
		for( int i = 0; i < N; i ++ )
		{
			data[ i * N + i ].copyIn( new DoubleType( 1.0 ) );
		}
		Matrix m = new MatrixD( data , N , N );*/
		double avg_dt = 0.0;
		try
		{
			PrintWriter writer = new PrintWriter( "jama.txt", "UTF-8" );
			for( int j = 1; j <= N; j++ )
			{
				int matrix_size = j * step;
				/*DoubleMatrix c = DoubleMatrix.rand( matrix_size, matrix_size );
				double[] temp = new double[ matrix_size * matrix_size ];
				for( int i = 0; i < matrix_size; i++ )
					for( int n = 0; n < matrix_size; n++ )
						temp[ i * matrix_size + n ] = c.get( i , n );
				init( matrix_size );*/
				//DoubleMatrix b = DoubleMatrix.rand( matrix_size, 1 );
				Jama.Matrix a = Jama.Matrix.random( matrix_size , matrix_size );
				for( int i = 1; i <= iter; i++ )
				{
					long t0 = System.currentTimeMillis();
					//org.jblas.Decompose.lu( c );
					//double det = calcDet( temp , matrix_size );
					a.lu();
					long t1 = System.currentTimeMillis();
					avg_dt += ( double ) ( t1 - t0 ) * 0.001;
				}
				writer.println( matrix_size + " " + avg_dt / iter );
				avg_dt = 0.0;
				//release();
			}
			writer.close();
		} catch( Exception e )
		{
			e.printStackTrace();
		}
	}
}
