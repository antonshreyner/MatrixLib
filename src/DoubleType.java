import java.nio.ByteBuffer;
/**
 * Created by �� on 04.07.2015.
 */
public class DoubleType implements Type
{
	private double val;
	private double convert( Type a )
	{
		return ByteBuffer.wrap( a.getByteCode() ).getDouble();
	}
	public DoubleType( double a )
	{
		val = a;
	}
	@Override
	public void copyIn( Type a )
	{
		val = convert( a );
	}
	@Override
	public Type copy()
	{
		return new DoubleType( val );
	}
	@Override
	public Type add( Type a )
	{
		return new DoubleType( val + convert( a ) );
	}
	@Override
	public Type sub( Type a )
	{
		return new DoubleType( val - convert( a ) );
	}
	@Override
	public double length()
	{
		return val;
	}
	@Override
	public Type normalize()
	{
		return new DoubleType( 1.0 );
	}
	@Override
	public Type scale( double k )
	{
		return new DoubleType( val * k );
	}
	@Override
	public Type mul( Type a )
	{
		return new DoubleType( val * convert( a ) );
	}
	@Override
	public Type div( Type a )
	{
		return new DoubleType( val / convert( a ) );
	}
	@Override
	public Type inverse()
	{
		return new DoubleType( 1.0 / val );
	}
	@Override
	public LinTypes getType()
	{
		return LinTypes.DOUBLE;
	}
	@Override
	public Type[] genArray( int n )
	{
		DoubleType[] out = new DoubleType[ n ];
		for( int i = 0; i < n; i ++ )
			out[ i ] = new DoubleType( 0.0 );
		return out;
	}
	@Override
	public Type genSame()
	{
		return new DoubleType( val );
	}
	@Override
	public void unit()
	{
		val = 1.0;
	}
	@Override
	public String toStr()
	{
		return Double.toString( val );
	}
	@Override
	public boolean isZero()
	{
		return Math.abs( val ) < 0.000001;
	}
	@Override
	public byte[] getByteCode()
	{
		byte[] bytes = new byte[8];
		ByteBuffer.wrap( bytes ).putDouble( val );
		return bytes;
	}
}
